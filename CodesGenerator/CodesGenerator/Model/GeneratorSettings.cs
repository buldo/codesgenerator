﻿namespace CodesGenerator.Model
{
    using System.Collections.Generic;

    internal class GeneratorSettings
    {
        public List<string> LocationNames { get; set; } = new List<string>();

        public bool UsePrefixes { get; set; }

        public uint MinNumCnt { get; set; }

        public uint MaxNumCnt { get; set; }
    }
}
