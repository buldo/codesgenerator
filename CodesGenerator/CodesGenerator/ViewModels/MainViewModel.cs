﻿namespace CodesGenerator.ViewModels
{
    using System;
    using System.Linq;

    using CodesGenerator.Model;

    using Prism.Commands;
    using Prism.Mvvm;

    public class MainViewModel : BindableBase
    {
        private readonly Generator _generator = new Generator();

        private string _locationNames;

        private bool _usePrefixes;

        private uint _minNumCnt = 3;

        private uint _maxNumCnt = 5;

        private string _generatedText;

        public string LocationNames
        {
            get
            {
                return _locationNames;
            }
            set
            {
                SetProperty(ref _locationNames, value);
            }
        }

        public bool UsePrefixes
        {
            get
            {
                return _usePrefixes;
            }
            set
            {
                SetProperty(ref _usePrefixes, value);
            }
        }

        public uint MinNumCnt
        {
            get
            {
                return _minNumCnt;
            }
            set
            {
                SetProperty(ref _minNumCnt, value);
            }
        }

        public uint MaxNumCnt
        {
            get
            {
                return _maxNumCnt;
            }
            set
            {
                SetProperty(ref _maxNumCnt, value);
            }
        }

        public string GeneratedText
        {
            get
            {
                return _generatedText;
            }
            set
            {
                SetProperty(ref _generatedText, value);
            }
        }

        public DelegateCommand GenerateCommand { get; }

        public DelegateCommand SaveCommand { get; }

        public MainViewModel()
        {
            GenerateCommand = new DelegateCommand(ExecuteGenerate);
            SaveCommand = new DelegateCommand(ExecuteSave);
        }

        private void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        private void ExecuteGenerate()
        {
            var settings = new GeneratorSettings
                               {
                                   MaxNumCnt = MaxNumCnt,
                                   MinNumCnt = MinNumCnt,
                                   UsePrefixes = UsePrefixes,
                                   LocationNames =
                                       LocationNames.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries)
                                           .Select(str => str.Trim())
                                           .Where(str => !string.IsNullOrWhiteSpace(str))
                                           .ToList()
                               };

            GeneratedText = _generator.Generate(settings);
        }
    }
}